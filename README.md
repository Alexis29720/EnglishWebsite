# README

Welcome on the repo of our English Website for the RASPBUINO website

# Information

They are some things you need to know about our project

## Raspberry part
This part was built by Stéphan because he had already worked with this nanocomputer

## Arduino part
This part was built by Alexis because he had already worked with this little computer

# Licence

This project is protected by a Creative Commons License that you can find if you click on this image [![License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
